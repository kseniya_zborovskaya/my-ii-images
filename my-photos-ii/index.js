//global variables
const photosElements = document.querySelectorAll('.photo');
addEvList(photosElements);

let photoObj = function (src, num, album) {
    let obj = {
        props: {
            src: src,
            num: num,
            album: album,
        }
    }
    return obj;
}

function addEvList(list) {
    for (elem of list) {
        elem.addEventListener('click', (event) => {
            let currElem = event.target;
            let elemObj = photoObj(currElem.getAttribute('src'), Math.ceil(Math.random()), 'IIAlbum');
            photoOnClick(elemObj);
        });
    }
}

function photoOnClick(currTarg) {
    localStorage.setItem('photo', JSON.stringify(currTarg));
    window.location.href='./photo.html';
}