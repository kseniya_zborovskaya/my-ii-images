//global variables
const main = document.querySelector('.main');
const photo = JSON.parse(localStorage.getItem('photo'));

function addImgOnPage(photo) {
    let newImg = document.createElement('img');
    newImg.setAttribute('src', photo.props.src);
    main.appendChild(newImg);
}

addImgOnPage(photo);