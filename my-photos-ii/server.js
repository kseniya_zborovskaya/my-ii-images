const express = require('express');
const app = express();
const path = require('path');

// Установка пути к статическим файлам
app.use(express.static(path.join(__dirname, 'index.html')));

// Запуск сервера на порту 3000
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server runs ${PORT}`);
});